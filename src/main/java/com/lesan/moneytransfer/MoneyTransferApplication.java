package com.lesan.moneytransfer;

import com.lesan.moneytransfer.db.AccountDAO;
import com.lesan.moneytransfer.db.ClientDAO;
import com.lesan.moneytransfer.db.TransferDAO;
import com.lesan.moneytransfer.resources.AccountResource;
import com.lesan.moneytransfer.resources.ClientResource;
import com.lesan.moneytransfer.resources.TransferResource;
import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jdbi3.bundles.JdbiExceptionsBundle;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.h2.H2DatabasePlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MoneyTransferApplication extends Application<MoneyTransferConfiguration> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MoneyTransferApplication.class);

    public static void main(final String[] args) throws Exception {
        LOGGER.info("Start application.");
        new MoneyTransferApplication().run(args);
    }

    @Override
    public String getName() {
        return "MoneyTransfer";
    }

    @Override
    public void initialize(final Bootstrap<MoneyTransferConfiguration> bootstrap) {
        bootstrap.addBundle(new JdbiExceptionsBundle());
    }

    @Override
    public void run(final MoneyTransferConfiguration configuration,
                    final Environment environment) {


        final Jdbi jdbi = initializeJdbi(configuration, environment);

        final ClientDAO clientDAO = initializeClientDAO(jdbi);
        final AccountDAO accountDAO = initializeAccountDAO(jdbi);
        final TransferDAO transferDAO = initializeTransferDAO(jdbi);

        final TransferResource transferResource = new TransferResource(transferDAO);
        final ClientResource clientResource = new ClientResource(clientDAO);
        final AccountResource accountResource = new AccountResource(accountDAO);

        environment.jersey().register(new JsonProcessingExceptionMapper(true));
        environment.jersey().register(transferResource);
        environment.jersey().register(clientResource);
        environment.jersey().register(accountResource);
    }

    private TransferDAO initializeTransferDAO(Jdbi jdbi) {
        final TransferDAO transferDAO = jdbi.onDemand(TransferDAO.class);
        transferDAO.createTableIfNotExists();
        return transferDAO;
    }

    private AccountDAO initializeAccountDAO(Jdbi jdbi) {
        final AccountDAO accountDAO = jdbi.onDemand(AccountDAO.class);
        accountDAO.createTableIfNotExists();
        return accountDAO;
    }

    private ClientDAO initializeClientDAO(Jdbi jdbi) {
        final ClientDAO clientDAO = jdbi.onDemand(ClientDAO.class);
        clientDAO.createTableIfNotExists();
        return clientDAO;
    }

    private Jdbi initializeJdbi(MoneyTransferConfiguration configuration, Environment environment) {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSourceFactory(), "h2");
        jdbi.installPlugin(new SqlObjectPlugin());
        jdbi.installPlugin(new H2DatabasePlugin());
        return jdbi;
    }

}
