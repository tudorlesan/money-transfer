package com.lesan.moneytransfer.resources;

import com.codahale.metrics.annotation.Timed;
import com.lesan.moneytransfer.core.Client;
import com.lesan.moneytransfer.db.ClientDAO;
import io.dropwizard.validation.Validated;
import org.jdbi.v3.core.statement.UnableToExecuteStatementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/clients")
@Produces(MediaType.APPLICATION_JSON)
public class ClientResource {

    private static final String CLIENT_NOT_FOUND_MSG = "Client not found";

    private final ClientDAO clientDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientResource.class);

    public ClientResource(ClientDAO clientDAO) {
        this.clientDAO = clientDAO;
    }

    @GET
    @Timed
    @Path("/{id}")
    public Client findById(@PathParam("id") long id) {
        Client client = clientDAO.findById(id);

        if (client == null) {
            LOGGER.warn("Client with id [{}] not found", id);
            throw new WebApplicationException(CLIENT_NOT_FOUND_MSG, Response.Status.NOT_FOUND);
        }
        LOGGER.info("Retrieved Client with id [{}]", client.getId());
        return client;
    }

    @GET
    @Timed
    public List<Client> findAll(@DefaultValue("1") @QueryParam("page") int page, @DefaultValue("20") @QueryParam("size") int size) {
        if(page < 1 || size < 1){
            LOGGER.warn("No client page returned. The page and size have to be greater than 0");
            throw new WebApplicationException("The page and size have to be greater than 0", Response.Status.BAD_REQUEST);
        }
        LOGGER.info("Returned client page [{}] of size [{}]", page, size);
        return clientDAO.findAllPaginated(page, size);
    }

    @POST
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public Client create(Client client) {
        try {
            long clientId = clientDAO.insert(client);
            LOGGER.info("New client created: [{} {}]", client.getFirstName(), client.getLastName());
            return clientDAO.findById(clientId);
            
        } catch (UnableToExecuteStatementException ex){
            LOGGER.warn("Client was not created because of duplicate email: [{}]", client.getEmailAddress());
            throw new WebApplicationException("Email already used. Unable to create client", Response.Status.CONFLICT);
        }
    }
}
