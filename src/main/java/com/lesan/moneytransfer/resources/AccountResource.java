package com.lesan.moneytransfer.resources;

import com.codahale.metrics.annotation.Timed;
import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.db.AccountDAO;
import org.jdbi.v3.core.statement.UnableToExecuteStatementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    private static final String ACCOUNT_NOT_FOUND_MSG = "Account not found";
    private final AccountDAO accountDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountResource.class);

    public AccountResource(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @GET
    @Timed
    @Path("/{id}")
    public Account findById(@PathParam("id") long id) {
        Account account = accountDAO.findByIdWithDetails(id);
        if (account == null) {
            LOGGER.warn("Account with id [{}] not found", id);
            throw new WebApplicationException(ACCOUNT_NOT_FOUND_MSG, Response.Status.NOT_FOUND);
        }
        LOGGER.info("Retrieved Account with id [{}]", account.getId());
        return account;
    }

    @GET
    @Timed
    public List<Account> findAll(@DefaultValue("1") @QueryParam("page") int page, @DefaultValue("20") @QueryParam("size") int size) {
        if(page < 1 || size < 1){
            LOGGER.warn("No account page returned. The page and size have to be greater than 0");
            throw new WebApplicationException("The page and size have to be greater than 0", Response.Status.BAD_REQUEST);
        }
        LOGGER.info("Returned account page [{}] of size [{}]", page, size);
        return accountDAO.findAllWithDetailsPaginated(page, size);
    }

    @POST
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public Account create(Account account) {
        try {
            long accountId = accountDAO.insert(account, account.getClient().getId());
            LOGGER.info("New account created for client with id: [{} - balance is {} {}]", account.getClient().getId(), account.getBalance(), account.getCurrency());
            return accountDAO.findByIdWithDetails(accountId);

        } catch (UnableToExecuteStatementException ex){
            LOGGER.warn("Account was not created because of invalid client id: [{}]", account.getClient().getId());
            throw new WebApplicationException("Invalid client id for account", Response.Status.BAD_REQUEST);
        }
    }
}