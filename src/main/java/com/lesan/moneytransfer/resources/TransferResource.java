package com.lesan.moneytransfer.resources;


import com.codahale.metrics.annotation.Timed;
import com.lesan.moneytransfer.core.Transfer;
import com.lesan.moneytransfer.db.TransferDAO;
import com.lesan.moneytransfer.db.exceptions.TransferCreationException;
import com.lesan.moneytransfer.db.exceptions.TransferExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/transfers")
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource {

    private static final String TRANSFER_NOT_FOUND_MSG = "Transfer not found";
    private final TransferDAO transferDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(TransferResource.class);

    public TransferResource(TransferDAO transferDAO) {
        this.transferDAO = transferDAO;
    }

    @GET
    @Timed
    @Path("/{id}")
    public Transfer findById(@PathParam("id") long id) {
        Transfer transfer = transferDAO.findByIdWithDetails(id);
        if (transfer == null) {
            LOGGER.warn("Transfer with id [{}] not found", id);
            throw new WebApplicationException(TRANSFER_NOT_FOUND_MSG, Response.Status.NOT_FOUND);
        }
        LOGGER.info("Retrieved Transfer with id [{}]", transfer.getId());
        return transfer;
    }

    @GET
    @Timed
    public List<Transfer> findAll(@DefaultValue("1") @QueryParam("page") int page, @DefaultValue("20") @QueryParam("size") int size) {
        if(page < 1 || size < 1){
            LOGGER.warn("No transfer page returned. The page and size have to be greater than 0");
            throw new WebApplicationException("The page and size have to be greater than 0", Response.Status.BAD_REQUEST);
        }
        LOGGER.info("Returned transfer page [{}] of size [{}]", page, size);
        return transferDAO.findAllWithDetailsPaginated(page, size);
    }

    @POST
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public Transfer create(Transfer transfer) {
        try {
            long transferId = transferDAO.insert(transfer, transfer.getSourceAccount().getId(),
                    transfer.getDestinationAccount().getId());
            LOGGER.info("New transfer created for account: [{} - {} {}]", transfer.getSourceAccount().getId(), transfer.getDescription(), transfer.getAmount());
            return transferDAO.findByIdWithDetails(transferId);
        } catch (TransferCreationException e) {
            LOGGER.warn("Transfer was not created: [{}]", e.getMessage());
            throw new WebApplicationException(e.getMessage(), Response.Status.CONFLICT);
        }
    }

    @GET
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/execution/{id}")
    public Transfer execute(@PathParam("id") long id) {
        try {
            Transfer executedTransfer = transferDAO.executeTransfer(id);
            LOGGER.info("Transfer executed for account: [{} - {} {}]", executedTransfer.getSourceAccount().getId(), executedTransfer.getDescription(), executedTransfer.getAmount());
            return executedTransfer;
        } catch (TransferExecutionException e) {
            LOGGER.warn("Transfer was not executed: [{}]", e.getMessage());
            throw new WebApplicationException(e.getMessage(), Response.Status.CONFLICT);
        }
    }

    @DELETE
    @Timed
    @Path("{id}")
    public Response delete(@PathParam("id") long id) {
        if (transferDAO.findById(id) == null) {
            LOGGER.warn("Transfer was with id [{}] not found", id);
            throw new WebApplicationException(TRANSFER_NOT_FOUND_MSG, Response.Status.NOT_FOUND);
        }
        transferDAO.delete(id);
        LOGGER.warn("Transfer was with id [{}] deleted", id);
        return Response.ok().build();
    }
}
