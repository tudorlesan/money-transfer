package com.lesan.moneytransfer.core;

import com.lesan.moneytransfer.core.enums.TransferStatus;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Objects;

public class Transfer {

    private long id;
    private String description;
    private BigDecimal amount;
    private Account sourceAccount;
    private Account destinationAccount;
    private TransferStatus status;
    private DateTime createdOn;
    private DateTime executedOn;

    public Transfer() {
    }

    public Transfer(long id, String description, BigDecimal amount, TransferStatus status) {
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.status = status;
    }

    public Transfer(String description, BigDecimal amount, TransferStatus status) {
        this.description = description;
        this.amount = amount;
        this.status = status;
    }

    public Transfer(String description, BigDecimal amount) {
        this.description = description;
        this.amount = amount;
        this.status = TransferStatus.CREATED;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(Account sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public Account getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(Account destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public TransferStatus getStatus() {
        return status;
    }

    public void setStatus(TransferStatus status) {
        this.status = status;
    }

    public DateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }

    public DateTime getExecutedOn() {
        return executedOn;
    }

    public void setExecutedOn(DateTime executedOn) {
        this.executedOn = executedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return id == transfer.id &&
                Objects.equals(description, transfer.description) &&
                Objects.equals(amount, transfer.amount) &&
                Objects.equals(sourceAccount, transfer.sourceAccount) &&
                Objects.equals(destinationAccount, transfer.destinationAccount) &&
                status == transfer.status &&
                Objects.equals(createdOn, transfer.createdOn) &&
                Objects.equals(executedOn, transfer.executedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, amount, sourceAccount, destinationAccount, status, createdOn, executedOn);
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", sourceAccount=" + sourceAccount +
                ", destinationAccount=" + destinationAccount +
                ", status=" + status +
                ", createdOn=" + createdOn +
                ", executedOn=" + executedOn +
                '}';
    }
}
