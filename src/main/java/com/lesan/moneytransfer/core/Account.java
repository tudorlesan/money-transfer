package com.lesan.moneytransfer.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.lesan.moneytransfer.core.enums.Currency;
import org.joda.time.DateTime;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;
import java.util.Objects;


public class Account {

    private long id;
    private Client client;
    private BigDecimal balance;
    private Currency currency;
    private DateTime createdOn;

    public Account(long id, BigDecimal balance, Currency currency) {
        this.id = id;
        this.balance = balance;
        this.currency = currency;
    }

    @ConstructorProperties({"id", "client", "balance, currency"})
    public Account(@JsonProperty("id") long id, @JsonProperty("client") Client client, @JsonProperty("balance") BigDecimal balance, @JsonProperty("currency") Currency currency) {
        this.id = id;
        this.client = client;
        this.balance = balance;
        this.currency = currency;
    }

    public Account() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public DateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                Objects.equals(client, account.client) &&
                Objects.equals(balance, account.balance) &&
                currency == account.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, client, balance, currency);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", client=" + client +
                ", balance=" + balance +
                ", currency=" + currency +
                ", createdOn=" + createdOn +
                '}';
    }
}
