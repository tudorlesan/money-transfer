package com.lesan.moneytransfer.core.exceptions;

public class CurrencyNotSupportedException extends Exception {
    public CurrencyNotSupportedException() {
        super("Selected currency is not supported.");
    }
}
