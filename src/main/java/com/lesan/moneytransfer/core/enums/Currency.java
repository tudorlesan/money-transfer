package com.lesan.moneytransfer.core.enums;

import com.lesan.moneytransfer.core.exceptions.CurrencyNotSupportedException;

import java.util.Arrays;

public enum Currency {
    GBP("gbp", "Pound sterling"), USD("usd", "United States Dollar"), EUR("eur", "Euro");

    private String code;
    private String name;

    Currency(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Currency findCurrencyByCode(String code) throws CurrencyNotSupportedException {
        return Arrays.stream(values()).filter(currency -> currency.code.equalsIgnoreCase(code)).findFirst().orElseThrow(CurrencyNotSupportedException::new);
    }
}
