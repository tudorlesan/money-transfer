package com.lesan.moneytransfer.core.enums;

public enum TransferStatus {
    CREATED, EXECUTED
}
