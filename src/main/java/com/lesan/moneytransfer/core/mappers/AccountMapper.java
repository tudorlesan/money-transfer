package com.lesan.moneytransfer.core.mappers;

import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.core.exceptions.CurrencyNotSupportedException;
import com.lesan.moneytransfer.core.enums.Currency;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountMapper implements RowMapper<Account> {

    @Override
    public Account map(ResultSet rs, StatementContext ctx) throws SQLException {
        try {
            return new Account(rs.getLong("id"), rs.getBigDecimal("balance"),
                    Currency.findCurrencyByCode(rs.getString("currencyCode")));
        } catch (CurrencyNotSupportedException e) {
            throw new SQLException(e.getMessage());
        }
    }
}