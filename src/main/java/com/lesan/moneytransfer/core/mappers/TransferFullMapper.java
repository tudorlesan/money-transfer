package com.lesan.moneytransfer.core.mappers;

import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.core.Client;
import com.lesan.moneytransfer.core.exceptions.CurrencyNotSupportedException;
import com.lesan.moneytransfer.core.Transfer;
import com.lesan.moneytransfer.core.enums.Currency;
import com.lesan.moneytransfer.core.enums.TransferStatus;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.joda.time.DateTime;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TransferFullMapper implements RowMapper<Transfer> {

    @Override
    public Transfer map(ResultSet rs, StatementContext ctx) throws SQLException {
        Account destinationAccount;
        Account sourceAccount;
        try {
            sourceAccount = new Account(rs.getLong("sa_id"), rs.getBigDecimal("sa_balance"),
                    Currency.findCurrencyByCode(rs.getString("sa_currency")));

            destinationAccount = new Account(rs.getLong("da_id"), rs.getBigDecimal("da_balance"),
                    Currency.findCurrencyByCode(rs.getString("da_currency")));
            if(rs.getDate("sa_createdOn") != null){
                sourceAccount.setCreatedOn(new DateTime(rs.getDate("sa_createdOn")));
            }
            if(rs.getDate("da_createdOn") != null){
                destinationAccount.setCreatedOn(new DateTime(rs.getDate("da_createdOn")));
            }

        } catch (CurrencyNotSupportedException e) {
            throw new SQLException(e.getMessage());
        }

        Transfer transfer = new Transfer(rs.getLong("id"), rs.getString("description"),
                rs.getBigDecimal("amount"), TransferStatus.valueOf(rs.getString("status")));
        transfer.setSourceAccount(sourceAccount);
        transfer.setDestinationAccount(destinationAccount);

        if(rs.getDate("t_createdOn") != null) {
            transfer.setCreatedOn(new DateTime(rs.getDate("t_createdOn")));
        }
        if(rs.getDate("t_executedOn") != null) {
            transfer.setExecutedOn(new DateTime(rs.getDate("t_executedOn")));
        }

        return transfer;
    }
}