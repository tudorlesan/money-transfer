package com.lesan.moneytransfer.core.mappers;


import com.lesan.moneytransfer.core.Client;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientMapper implements RowMapper<Client> {

    @Override
    public Client map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Client(rs.getLong("id"), rs.getString("firstName"),
                rs.getString("lastName"), rs.getString("emailAddress"));
    }
}