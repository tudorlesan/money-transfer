package com.lesan.moneytransfer.core.mappers;

import com.lesan.moneytransfer.core.Transfer;
import com.lesan.moneytransfer.core.enums.TransferStatus;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TransferMapper implements RowMapper<Transfer> {

    @Override
    public Transfer map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Transfer(rs.getLong("id"), rs.getString("description"),
                rs.getBigDecimal("amount"), TransferStatus.valueOf(rs.getString("status")));
    }
}