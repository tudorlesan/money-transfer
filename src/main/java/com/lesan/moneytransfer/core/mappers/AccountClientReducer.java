package com.lesan.moneytransfer.core.mappers;

import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.core.Client;
import org.jdbi.v3.core.result.LinkedHashMapRowReducer;
import org.jdbi.v3.core.result.RowView;

import java.util.Map;

public class AccountClientReducer implements LinkedHashMapRowReducer<Integer, Account> {

    @Override
    public void accumulate(Map<Integer, Account> container, RowView rowView) {
        final Account account = container.computeIfAbsent(rowView.getColumn("a_id", Integer.class),
                id -> rowView.getRow(Account.class));

        if (rowView.getColumn("c_id", Integer.class) != null) {
            account.setClient(rowView.getRow(Client.class));
        }
    }
}