package com.lesan.moneytransfer.db;

import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.core.Client;
import com.lesan.moneytransfer.core.mappers.AccountClientReducer;
import com.lesan.moneytransfer.core.mappers.AccountMapper;
import org.jdbi.v3.sqlobject.SqlObject;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.Timestamped;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowReducer;

import java.util.List;

@RegisterRowMapper(AccountMapper.class)
public interface AccountDAO extends SqlObject {

    @SqlUpdate("create table if not exists accounts (id identity not null primary key, clientId long not null, " +
            "balance decimal not null, currency varchar(10) not null, createdOn datetime, foreign key (clientId) references clients(id))")
    void createTableIfNotExists();

    @SqlUpdate("insert into accounts (clientId, balance, currency, createdOn) values (:clientId, :balance, :currency, :now)")
    @GetGeneratedKeys("id")
    @Timestamped
    long insert(@BindBean Account account, @Bind("clientId") long clientId);


    @RegisterBeanMapper(value = Account.class, prefix = "a")
    @RegisterBeanMapper(value = Client.class, prefix = "c")
    @SqlQuery("select accounts.id a_id, accounts.balance a_balance, accounts.currency a_currency, accounts.createdOn a_createdOn, " +
            "clients.id c_id, clients.firstName c_firstName, clients.lastName c_lastName, clients.emailAddress c_emailAddress " +
            "from accounts inner join clients on accounts.clientId = clients.id")
    @UseRowReducer(AccountClientReducer.class)
    List<Account> findAllWithDetails();

    @RegisterBeanMapper(value = Account.class, prefix = "a")
    @RegisterBeanMapper(value = Client.class, prefix = "c")
    @SqlQuery("select accounts.id a_id, accounts.balance a_balance, accounts.currency a_currency, accounts.createdOn a_createdOn, " +
            "clients.id c_id, clients.firstName c_firstName, clients.lastName c_lastName, clients.emailAddress c_emailAddress " +
            "from accounts inner join clients on accounts.clientId = clients.id limit :size offset :page-1")
    @UseRowReducer(AccountClientReducer.class)
    List<Account> findAllWithDetailsPaginated(@Bind("page") int page, @Bind("size") int size);

    @RegisterBeanMapper(value = Account.class, prefix = "a")
    @RegisterBeanMapper(value = Client.class, prefix = "c")
    @SqlQuery("select accounts.id a_id, accounts.balance a_balance, accounts.currency a_currency, accounts.createdOn a_createdOn, " +
            "clients.id c_id, clients.firstName c_firstName, clients.lastName c_lastName, clients.emailAddress c_emailAddress " +
            "from accounts inner join clients on accounts.clientId = clients.id " +
            "where accounts.id = :id")
    @UseRowReducer(AccountClientReducer.class)
    Account findByIdWithDetails(@Bind("id") long id);

}