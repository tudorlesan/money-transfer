package com.lesan.moneytransfer.db;


import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.core.Transfer;
import com.lesan.moneytransfer.core.enums.TransferStatus;
import com.lesan.moneytransfer.core.mappers.TransferFullMapper;
import com.lesan.moneytransfer.core.mappers.TransferMapper;
import com.lesan.moneytransfer.db.exceptions.TransferCreationException;
import com.lesan.moneytransfer.db.exceptions.TransferExecutionException;
import org.jdbi.v3.core.transaction.TransactionIsolationLevel;
import org.jdbi.v3.sqlobject.SqlObject;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.Timestamped;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.math.BigDecimal;
import java.util.List;

import static com.lesan.moneytransfer.db.exceptions.Constants.*;


@RegisterRowMapper(TransferMapper.class)
public interface TransferDAO extends SqlObject {

    @SqlUpdate("create table if not exists transfers (id identity not null primary key, description varchar(200) not null, " +
            "amount decimal not null, sourceAccountId long not null, destinationAccountId long not null, status varchar(20) not null, " +
            "createdOn datetime, executedOn datetime, foreign key (sourceAccountId) " +
            "references accounts(id), foreign key (destinationAccountId) references accounts(id))")
    void createTableIfNotExists();

    @SqlQuery("select * from transfers where id = ?")
    Transfer findById(long id);

    @SqlQuery("select t.id t_id, t.description t_description, t.amount t_amount, t.status t_status, t.createdOn t_createdOn, t.executedOn t_executedOn, " +
            "sa.id sa_id, sa.balance sa_balance, sa.currency sa_currency, sa.createdOn sa_createdOn, " +
            "da.id da_id, da.balance da_balance, da.currency da_currency, da.createdOn da_createdOn  " +
            "from transfers t inner join accounts sa on t.sourceAccountId  = sa.id " +
            "inner join accounts da on t.destinationAccountId = da.id where t.id = :id")
    @RegisterRowMapper(TransferFullMapper.class)
    Transfer findByIdWithDetails(@Bind("id") long id);

    @SqlQuery("select t.id t_id, t.description t_description, t.amount t_amount, t.status t_status, t.createdOn t_createdOn, t.executedOn t_executedOn, " +
            "sa.id sa_id, sa.balance sa_balance, sa.currency sa_currency, sa.clientId sa_clientId, sa.createdOn sa_createdOn, " +
            "da.id da_id, da.balance da_balance, da.currency da_currency, da.clientId da_clientId, da.createdOn da_createdOn " +
            "from transfers t inner join accounts sa on t.sourceAccountId  = sa.id " +
            "inner join accounts da on t.destinationAccountId = da.id")
    @RegisterRowMapper(TransferFullMapper.class)
    List<Transfer> findAllWithDetails();

    @SqlQuery("select t.id t_id, t.description t_description, t.amount t_amount, t.status t_status, t.createdOn t_createdOn, t.executedOn t_executedOn, " +
            "sa.id sa_id, sa.balance sa_balance, sa.currency sa_currency, sa.clientId sa_clientId, sa.createdOn sa_createdOn, " +
            "da.id da_id, da.balance da_balance, da.currency da_currency, da.clientId da_clientId, da.createdOn da_createdOn " +
            "from transfers t inner join accounts sa on t.sourceAccountId  = sa.id " +
            "inner join accounts da on t.destinationAccountId = da.id limit :size offset :page-1")
    @RegisterRowMapper(TransferFullMapper.class)
    List<Transfer> findAllWithDetailsPaginated(@Bind("page") int page, @Bind("size") int size);

    @SqlUpdate("insert into transfers (description, amount, sourceAccountId, destinationAccountId, status, createdOn) values " +
            "(:description, :amount, :sourceAccountId, :destinationAccountId, :status, :now)")
    @GetGeneratedKeys("id")
    @Timestamped
    long insertTransfer(@BindBean Transfer transfer, @Bind("sourceAccountId") long sourceAccountId, @Bind("destinationAccountId") long destinationAccountId);


    @Transaction
    default long insert(Transfer transfer, long sourceAccountId, long destinationAccountId) throws TransferCreationException {
        if (sourceAccountId == destinationAccountId) {
            throw new TransferCreationException(TRANSFER_BETWEEN_SAME_ACCOUNT);
        }

        Account sourceAccount;
        Account destinationAccount;
        try {
            sourceAccount = getHandle().createQuery("select * from accounts where id = " + sourceAccountId)
                    .mapToBean(Account.class).first();
            destinationAccount = getHandle().createQuery("select * from accounts where id = " + destinationAccountId).
                    mapToBean(Account.class).first();
        } catch (IllegalStateException e) {
            throw new TransferCreationException(TRANSFER_BETWEEN_INEXISTENT_ACCOUNTS);
        }

        if (sourceAccount.getCurrency() != destinationAccount.getCurrency()) {
            throw new TransferCreationException(TRANSFER_BETWEEN_INCOMPATIBLE_ACCOUNTS);
        }
        transfer.setStatus(TransferStatus.CREATED);
        return insertTransfer(transfer, sourceAccountId, destinationAccountId);

    }

    @Transaction(value= TransactionIsolationLevel.SERIALIZABLE)
    default Transfer executeTransfer(long id) throws TransferExecutionException {

        Transfer transferFromDb = findByIdWithDetails(id);
        if (transferFromDb == null) {
            throw new TransferExecutionException(TRANSFER_NOT_FOUND_MSG);
        }
        if (transferFromDb.getStatus() == TransferStatus.EXECUTED) {
            throw new TransferExecutionException(TRANSFER_ALREADY_EXECUTED_MSG + transferFromDb.getExecutedOn().toLocalDate());
        }
        if (transferFromDb.getSourceAccount().getBalance().compareTo(transferFromDb.getAmount()) < 0) {
            throw new TransferExecutionException(TRANSFER_INSUFFICIENT_FUNDS_MSG);
        }

        actualizeBalance(transferFromDb.getSourceAccount(), transferFromDb.getAmount().multiply(new BigDecimal("-1")));
        actualizeBalance(transferFromDb.getDestinationAccount(), transferFromDb.getAmount());

        transferFromDb.setStatus(TransferStatus.EXECUTED);
        updateStatus(transferFromDb.getId(), transferFromDb.getStatus());

        transferFromDb = findByIdWithDetails(id);
        return transferFromDb;
    }

    default void actualizeBalance(Account account, BigDecimal amount) {
        getHandle().execute("update accounts set balance = balance + ? where id = ?", amount, account.getId());
    }

    @SqlUpdate("update transfers set description = :description, amount = :amount, status = :status where id = :id")
    void update(@BindBean Transfer transfer);

    @Timestamped
    @SqlUpdate("update transfers set status = :status, executedOn = :now where id = :id")
    void updateStatus(@Bind("id") long id, @Bind("status") TransferStatus status);

    @SqlUpdate("delete from transfers where id = :id")
    void delete(@Bind("id") long id);

}
