package com.lesan.moneytransfer.db;

import com.lesan.moneytransfer.core.Client;
import com.lesan.moneytransfer.core.mappers.ClientMapper;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

@RegisterRowMapper(ClientMapper.class)
public interface ClientDAO {

    @SqlUpdate("create table if not exists clients (id identity not null primary key, firstName varchar(100) not null, " +
            "lastName varchar(100) not null, emailAddress varchar(100) not null unique)")
    void createTableIfNotExists();

    @SqlQuery("select * from clients where id = ?")
    Client findById(long id);

    @SqlQuery("SELECT * FROM clients LIMIT :size OFFSET :page-1")
    List<Client> findAllPaginated(@Bind("page") int page, @Bind("size") int size);

    @SqlQuery("SELECT * FROM clients")
    List<Client> findAll();

    @SqlUpdate("insert into clients (firstName, lastName, emailAddress) values (:firstName, :lastName, :emailAddress)")
    @GetGeneratedKeys("id")
    long insert(@BindBean Client client);

}