package com.lesan.moneytransfer.db.exceptions;

public class TransferExecutionException extends Throwable {
    public TransferExecutionException(String msg) {
        super(msg);
    }
}
