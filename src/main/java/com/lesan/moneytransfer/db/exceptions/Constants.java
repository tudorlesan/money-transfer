package com.lesan.moneytransfer.db.exceptions;

public final class Constants {
    public static final String TRANSFER_NOT_FOUND_MSG = "Transfer not found";
    public static final String TRANSFER_ALREADY_EXECUTED_MSG = "Transfer was already executed on: ";
    public static final String TRANSFER_INSUFFICIENT_FUNDS_MSG = "Source account has insufficient funds to execute the transfer";
    public static final String TRANSFER_BETWEEN_INCOMPATIBLE_ACCOUNTS = "Source account and destination account have different currencies. " +
                                                                        "Transfer between accounts with different currencies is not supported at the moment";
    public static final String TRANSFER_BETWEEN_SAME_ACCOUNT = "Source account matches destination account. Transfer cannot be created";
    public static final String TRANSFER_BETWEEN_INEXISTENT_ACCOUNTS = "Account not found";
    private Constants() { }
}
