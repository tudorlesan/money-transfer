package com.lesan.moneytransfer.db.exceptions;

public class TransferCreationException extends Throwable {
    public TransferCreationException(String msg) {
        super(msg);
    }
}
