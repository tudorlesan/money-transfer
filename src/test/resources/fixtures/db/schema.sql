create table if not exists clients (id identity not null primary key, firstName varchar(100) not null,
lastName varchar(100) not null, emailAddress varchar(100) not null unique);

create table if not exists accounts (id identity not null primary key, clientId long not null,
balance decimal not null, currency varchar(10) not null, createdOn datetime, foreign key (clientId) references clients(id));

create table if not exists transfers (id identity not null primary key, description varchar(200) not null,
amount decimal not null, sourceAccountId long not null, destinationAccountId long not null, status varchar(20) not null,
createdOn datetime, executedOn datetime, foreign key (sourceAccountId) references accounts(id),
foreign key (destinationAccountId) references accounts(id));

insert into clients (firstName, lastName, emailAddress) values
('Test1', 'User1', 'test1@user.com'),
('Test2', 'User2', 'test2@user.com'),
('Test3', 'User3', 'test3@user.com'),
('Test4', 'User4', 'test4@user.com'),
('Test5', 'User5', 'test5@user.com');

insert into accounts (clientId, balance, currency) values
(1, 100, 'GBP'),
(1, 200.07, 'EUR'),
(2, 1300.97, 'USD'),
(3, 25933, 'USD'),
(3, 436.35, 'GBP'),
(3, 9573.2, 'EUR'),
(4, 25432.4, 'EUR'),
(5, 0, 'GBP');


insert into transfers (description, amount, sourceAccountId, destinationAccountId, status) values
('test transfer 1', 50, 1, 5, 'CREATED'),
('test transfer 2', 123.5, 6, 2, 'CREATED'),
('test transfer 3', 1333.45, 5, 8, 'CREATED'),
('test transfer 4', 923.35, 3, 4, 'CREATED'),
('test transfer 5', 5543.5, 8, 1, 'CREATED');



