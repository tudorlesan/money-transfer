package com.lesan.moneytransfer.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lesan.moneytransfer.core.enums.Currency;
import com.lesan.moneytransfer.core.enums.TransferStatus;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;

import java.math.BigDecimal;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

public class TransferTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private static final String FIXTURES_TRANSFER_JSON = "fixtures/transfer.json";

    @Test
    public void serializesToJson() throws Exception {

        final Transfer transfer = getTransfer();
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture(FIXTURES_TRANSFER_JSON), Transfer.class));

        assertThat(MAPPER.writeValueAsString(transfer)).isEqualTo(expected);

    }

    @Test
    public void deserializesFromJSON() throws Exception {
        final Transfer transfer = getTransfer();
        assertThat(MAPPER.readValue(fixture(FIXTURES_TRANSFER_JSON), Transfer.class))
                .isEqualTo(transfer);
    }


    private static Transfer getTransfer() {
        Client c = new Client(1, "John", "Doe", "johndoe@gmail.com");
        Account sa = new Account(1, c, new BigDecimal("100.05"), Currency.GBP);

        Account da = new Account(2, c, new BigDecimal("200.15"), Currency.GBP);
        Transfer t = new Transfer(1, "test transfer", new BigDecimal("50"), TransferStatus.CREATED);
        t.setSourceAccount(sa);
        t.setDestinationAccount(da);
        return t;
    }
}