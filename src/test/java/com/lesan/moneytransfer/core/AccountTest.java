package com.lesan.moneytransfer.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lesan.moneytransfer.core.enums.Currency;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;

import java.math.BigDecimal;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private static final String FIXTURES_ACCOUNT_JSON = "fixtures/account.json";

    @Test
    public void serializesToJson() throws Exception {

        final Account account = getAccount();
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture(FIXTURES_ACCOUNT_JSON), Account.class));

        assertThat(MAPPER.writeValueAsString(account)).isEqualTo(expected);

    }

    @Test
    public void deserializesFromJSON() throws Exception {
        final Account account = getAccount();
        assertThat(MAPPER.readValue(fixture(FIXTURES_ACCOUNT_JSON), Account.class))
                .isEqualTo(account);
    }


    private static Account getAccount() {
        Client c = new Client(1, "John", "Doe", "johndoe@gmail.com");
        return new Account(1, c, new BigDecimal("100.05"), Currency.GBP);
    }
}
