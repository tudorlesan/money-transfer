package com.lesan.moneytransfer.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

public class ClientTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private static final String FIXTURES_CLIENT_JSON = "fixtures/client.json";

    @Test
    public void serializesToJson() throws Exception {

        final Client client = getClient();
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture(FIXTURES_CLIENT_JSON), Client.class));

        assertThat(MAPPER.writeValueAsString(client)).isEqualTo(expected);

    }

    @Test
    public void deserializesFromJSON() throws Exception {
        final Client client = getClient();
        assertThat(MAPPER.readValue(fixture(FIXTURES_CLIENT_JSON), Client.class))
                .isEqualTo(client);
    }


    private static Client getClient() {
        Client c = new Client("John", "Doe", "johndoe@gmail.com");
        c.setId(1);
        return c;
    }
}
