package com.lesan.moneytransfer.db;

import com.codahale.metrics.MetricRegistry;
import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.core.Transfer;
import com.lesan.moneytransfer.core.enums.Currency;
import com.lesan.moneytransfer.core.enums.TransferStatus;
import com.lesan.moneytransfer.db.exceptions.Constants;
import com.lesan.moneytransfer.db.exceptions.TransferCreationException;
import com.lesan.moneytransfer.db.exceptions.TransferExecutionException;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import io.dropwizard.testing.FixtureHelpers;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferDAOTest {

    private static final int TEST_ID_1 = 1;
    private static final int TEST_ID_2 = 2;
    private static final int TEST_ID_NOT_FOUND = -999;
    private static final int TRANSFER_POS_WITH_INSUFFICIENT_FUNDS = 2;
    private static final int TRANSFER_POS_EXECUTABLE = 0;
    private static final String NEW_TRANSFER_DESCRIPTION = "new transfer";
    private static final String NEW_TRANSFER_AMOUNT = "100";

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    private TransferDAO transferDAO;

    private static List<Transfer> transferList;
    private static Jdbi jdbi;
    private static Handle handle;

    @BeforeClass
    public static void setUp() {
        Environment env = new Environment("test-env", Jackson.newObjectMapper(), null, new MetricRegistry(), null);

        JdbiFactory factory = new JdbiFactory();
        jdbi = factory.build(env, getDataSourceFactory(), "test");

        handle = jdbi.open();

    }

    @AfterClass
    public static void tearDown(){
        handle.close();
    }

    private static DataSourceFactory getDataSourceFactory() {
        DataSourceFactory dataSourceFactory = new DataSourceFactory();
        dataSourceFactory.setDriverClass("org.h2.Driver");
        dataSourceFactory.setUrl("jdbc:h2:mem:testDb");
        dataSourceFactory.setUser("sa");
        dataSourceFactory.setPassword("");
        return dataSourceFactory;
    }

    @Before
    public void setupTests() {
        transferDAO = jdbi.onDemand(TransferDAO.class);
        createDatabase();
        initializeInternalList();

    }

    @After
    public void tearDownTests() {
        resetDatabase();
    }


    @Test
    public void testFindById() {
        assertThat(transferDAO.findByIdWithDetails(TEST_ID_1)).isEqualTo(transferList.get(0));
    }

    @Test
    public void testFindByIdNotFound() {
        assertThat(transferDAO.findByIdWithDetails(TEST_ID_NOT_FOUND)).isNull();
    }

    @Test
    public void testFindAll() {
        assertThat(transferDAO.findAllWithDetails()).isEqualTo(getTransferList());
    }

    @Test
    public void testFindAllPaginated() {
        List<Transfer> result = transferDAO.findAllWithDetailsPaginated(1, 1);
        assertThat(result).isEqualTo(Collections.singletonList(transferList.get(0)));

        result = transferDAO.findAllWithDetailsPaginated(2, 1);
        assertThat(result).isEqualTo(Collections.singletonList(transferList.get(1)));

        result = transferDAO.findAllWithDetailsPaginated(1, 2);
        assertThat(result).isEqualTo(new ArrayList<>(Arrays.asList(transferList.get(0), transferList.get(1))));

        result = transferDAO.findAllWithDetailsPaginated(1, transferList.size());
        assertThat(result).isEqualTo(transferList);
    }

    @Test
    public void testFindAllPaginatedInvalidInput() {
        List<Transfer> result = transferDAO.findAllWithDetailsPaginated(-20, 1);
        assertThat(result).isEqualTo(Collections.singletonList(transferList.get(0)));

        result = transferDAO.findAllWithDetailsPaginated(0, 1);
        assertThat(result).isEqualTo(Collections.singletonList(transferList.get(0)));

        result = transferDAO.findAllWithDetailsPaginated(2, 0);
        assertThat(result).isEqualTo(Collections.emptyList());

        result = transferDAO.findAllWithDetailsPaginated(999, 1);
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    public void testInsertTransferSuccess() throws TransferCreationException {
        Account sourceAccount = getTransferList().get(0).getSourceAccount();
        Account destinationAccount = getTransferList().get(0).getDestinationAccount();
        Transfer newTransfer = new Transfer(NEW_TRANSFER_DESCRIPTION, new BigDecimal(NEW_TRANSFER_AMOUNT));
        newTransfer.setSourceAccount(sourceAccount);
        newTransfer.setDestinationAccount(destinationAccount);

        long id = transferDAO.insert(newTransfer, newTransfer.getSourceAccount().getId(), newTransfer.getDestinationAccount().getId());

        Transfer addedTransfer = transferDAO.findByIdWithDetails(id);
        assertThat(addedTransfer).isNotNull();
        assertThat(addedTransfer.getId()).isEqualTo(id);
        assertThat(addedTransfer.getSourceAccount()).isEqualTo(sourceAccount);
        assertThat(addedTransfer.getDestinationAccount()).isEqualTo(destinationAccount);
        assertThat(addedTransfer.getDescription()).isEqualTo(newTransfer.getDescription());
        assertThat(addedTransfer.getAmount()).isEqualTo(newTransfer.getAmount());
        assertThat(addedTransfer.getStatus()).isEqualTo(newTransfer.getStatus());
        assertThat(addedTransfer.getCreatedOn()).isNotNull();

        transferDAO.delete(id);
    }

    @Test
    public void testInsertTransferInvalidAccount() throws TransferCreationException {
        expectedEx.expect(TransferCreationException.class);
        expectedEx.expectMessage(Constants.TRANSFER_BETWEEN_INEXISTENT_ACCOUNTS);

        Transfer newTransfer = new Transfer(NEW_TRANSFER_DESCRIPTION, new BigDecimal(NEW_TRANSFER_AMOUNT));
        transferDAO.insert(newTransfer, TEST_ID_NOT_FOUND, TEST_ID_NOT_FOUND+1);
    }

    @Test
    public void testInsertTransferSameAccounts() throws TransferCreationException {
        expectedEx.expect(TransferCreationException.class);
        expectedEx.expectMessage(Constants.TRANSFER_BETWEEN_SAME_ACCOUNT);

        Transfer newTransfer = new Transfer(NEW_TRANSFER_DESCRIPTION, new BigDecimal(NEW_TRANSFER_AMOUNT));
        transferDAO.insert(newTransfer, TEST_ID_1, TEST_ID_1);
    }

    @Test
    public void testInsertTransferIncompatibleAccounts() throws TransferCreationException {
        expectedEx.expect(TransferCreationException.class);
        expectedEx.expectMessage(Constants.TRANSFER_BETWEEN_INCOMPATIBLE_ACCOUNTS);

        Transfer newTransfer = new Transfer(NEW_TRANSFER_DESCRIPTION, new BigDecimal(NEW_TRANSFER_AMOUNT));
        transferDAO.insert(newTransfer, TEST_ID_1, TEST_ID_2);
    }


    @Test
    public void testExecuteTransferInvalid() throws TransferExecutionException {
        expectedEx.expect(TransferExecutionException.class);
        expectedEx.expectMessage(Constants.TRANSFER_NOT_FOUND_MSG);

        transferDAO.executeTransfer(TEST_ID_NOT_FOUND);
    }

    @Test
    public void testExecuteTransferAlreadyExecuted() throws TransferExecutionException {
        expectedEx.expect(TransferExecutionException.class);
        expectedEx.expectMessage(Constants.TRANSFER_ALREADY_EXECUTED_MSG);

        Transfer transferToExecute = transferList.get(TRANSFER_POS_EXECUTABLE);
        transferDAO.executeTransfer(transferToExecute.getId());
        transferDAO.executeTransfer(transferToExecute.getId());
    }

    @Test
    public void testExecuteTransferInsufficientFunds() throws TransferExecutionException {
        expectedEx.expect(TransferExecutionException.class);
        expectedEx.expectMessage(Constants.TRANSFER_INSUFFICIENT_FUNDS_MSG);

        Transfer transferToExecute = transferList.get(TRANSFER_POS_WITH_INSUFFICIENT_FUNDS);
        transferDAO.executeTransfer(transferToExecute.getId());
    }

    @Test()
    public void testExecuteTransferSuccess() throws TransferExecutionException {

        Transfer transferToExecute = transferList.get(TRANSFER_POS_EXECUTABLE);
        BigDecimal sourceBalanceBeforeTransfer = transferToExecute.getSourceAccount().getBalance();
        BigDecimal destinationBalanceBeforeTransfer = transferToExecute.getDestinationAccount().getBalance();
        BigDecimal amountToTransfer = transferToExecute.getAmount();

        Transfer executedTransfer = transferDAO.executeTransfer(transferToExecute.getId());

        assertThat(executedTransfer).isNotNull();
        assertThat(executedTransfer.getStatus()).isEqualTo(TransferStatus.EXECUTED);
        assertThat(executedTransfer.getExecutedOn()).isNotNull();

        assertThat(executedTransfer.getSourceAccount().getBalance()).isEqualTo(sourceBalanceBeforeTransfer.subtract(amountToTransfer));
        assertThat(executedTransfer.getDestinationAccount().getBalance()).isEqualTo(destinationBalanceBeforeTransfer.add(amountToTransfer));
    }



    private static void createDatabase() {
        handle.begin();
        handle.createScript(FixtureHelpers.fixture("fixtures/db/schema.sql"))
                .execute();
        handle.commit();
    }

    private void resetDatabase() {
        handle.createScript(FixtureHelpers.fixture("fixtures/db/reset.sql"))
                .execute();
        handle.commit();
    }

    private static List<Transfer> getTransferList() {
        return transferList;
    }

    private void initializeInternalList() {

        List<Account> accountList = new ArrayList<>(Arrays.asList(
                new Account(1, null, new BigDecimal("100"), Currency.GBP),
                new Account(2, null, new BigDecimal("200.07"), Currency.EUR),
                new Account(3, null, new BigDecimal("1300.97"), Currency.USD),
                new Account(4, null, new BigDecimal("25933"), Currency.USD),
                new Account(5, null, new BigDecimal("436.35"), Currency.GBP),
                new Account(6, null, new BigDecimal("9573.2"), Currency.EUR),
                new Account(7, null, new BigDecimal("25432.4"), Currency.EUR),
                new Account(8, null, new BigDecimal("0"), Currency.GBP)
        ));

        Transfer t1 = new Transfer(1, "test transfer 1", new BigDecimal("50"), TransferStatus.CREATED);
        t1.setSourceAccount(accountList.get(0));
        t1.setDestinationAccount(accountList.get(4));
        Transfer t2 = new Transfer(2, "test transfer 2", new BigDecimal("123.5"), TransferStatus.CREATED);
        t2.setSourceAccount(accountList.get(5));
        t2.setDestinationAccount(accountList.get(1));
        Transfer t3 = new Transfer(3, "test transfer 3", new BigDecimal("1333.45"), TransferStatus.CREATED);
        t3.setSourceAccount(accountList.get(4));
        t3.setDestinationAccount(accountList.get(7));
        Transfer t4 = new Transfer(4, "test transfer 4", new BigDecimal("923.35"), TransferStatus.CREATED);
        t4.setSourceAccount(accountList.get(2));
        t4.setDestinationAccount(accountList.get(3));
        Transfer t5 = new Transfer(5, "test transfer 5", new BigDecimal("5543.5"), TransferStatus.CREATED);
        t5.setSourceAccount(accountList.get(7));
        t5.setDestinationAccount(accountList.get(0));
        transferList = new ArrayList<>(Arrays.asList(t1, t2, t3, t4,t5));
    }
}
