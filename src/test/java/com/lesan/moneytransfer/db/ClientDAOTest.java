package com.lesan.moneytransfer.db;

import com.codahale.metrics.MetricRegistry;
import com.lesan.moneytransfer.core.Client;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import io.dropwizard.testing.FixtureHelpers;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.UnableToExecuteStatementException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ClientDAOTest {

    private static final int TEST_ID_1 = 1;
    private static final int TEST_ID_2 = 2;
    private static final int TEST_ID_3 = 3;
    private static final int TEST_ID_NOT_FOUND = -999;

    private ClientDAO clientDAO;

    private static List<Client> clientList;
    private static Jdbi jdbi;
    private static Handle handle;

    @BeforeClass
    public static void setUp() {
        Environment env = new Environment("test-env", Jackson.newObjectMapper(), null, new MetricRegistry(), null);

        JdbiFactory factory = new JdbiFactory();
        jdbi = factory.build(env, getDataSourceFactory(), "test");
        handle = jdbi.open();
        createDatabase();
    }

    @AfterClass
    public static void tearDown(){
        resetDatabase();
        handle.close();
    }

    private static DataSourceFactory getDataSourceFactory() {
        DataSourceFactory dataSourceFactory = new DataSourceFactory();
        dataSourceFactory.setDriverClass("org.h2.Driver");
        dataSourceFactory.setUrl("jdbc:h2:mem:testDb");
        dataSourceFactory.setUser("sa");
        dataSourceFactory.setPassword("");
        return dataSourceFactory;
    }

    @Before
    public void setupTests() {
        clientDAO = jdbi.onDemand(ClientDAO.class);
        initializeInternalList();
    }

    @Test
    public void testFindById() {
        assertThat(clientDAO.findById(TEST_ID_1)).isEqualTo(getClient(TEST_ID_1));
    }

    @Test
    public void testFindByIdNotFound() {
        assertThat(clientDAO.findById(TEST_ID_NOT_FOUND)).isNull();
    }

    @Test
    public void testFindAll() {
        assertThat(clientDAO.findAll()).isEqualTo(getClientList());
    }

    @Test
    public void testFindAllPaginated() {
        List<Client> result = clientDAO.findAllPaginated(1, 1);
        assertThat(result).isEqualTo(Collections.singletonList(getClient(TEST_ID_1)));

        result = clientDAO.findAllPaginated(2, 1);
        assertThat(result).isEqualTo(Collections.singletonList(getClient(TEST_ID_2)));

        result = clientDAO.findAllPaginated(2, 2);
        assertThat(result).isEqualTo(new ArrayList<>(Arrays.asList(getClient(TEST_ID_2), getClient(TEST_ID_3))));
    }

    @Test
    public void testFindAllPaginatedInvalidInput() {
        List<Client> result = clientDAO.findAllPaginated(-20, 1);
        assertThat(result).isEqualTo(Collections.singletonList(getClient(TEST_ID_1)));

        result = clientDAO.findAllPaginated(0, 1);
        assertThat(result).isEqualTo(Collections.singletonList(getClient(TEST_ID_1)));

        result = clientDAO.findAllPaginated(2, 0);
        assertThat(result).isEqualTo(Collections.emptyList());

        result = clientDAO.findAllPaginated(999, 1);
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    public void testInsertClientSuccess() {
        Client newClient = new Client("New", "Client", "new@client.com");
        long id = clientDAO.insert(newClient);

        Client addedClient = clientDAO.findById(id);
        assertThat(addedClient).isNotNull();
        assertThat(addedClient.getId()).isEqualTo(id);
        assertThat(addedClient.getFirstName()).isEqualTo(newClient.getFirstName());
        assertThat(addedClient.getLastName()).isEqualTo(newClient.getLastName());
        assertThat(addedClient.getEmailAddress()).isEqualTo(newClient.getEmailAddress());
        handle.execute("delete from clients where id = (?)", id);
        handle.commit();
    }

    @Test(expected = UnableToExecuteStatementException.class)
    public void testInsertClientDuplicateEmail() {
        Client alreadyInsertedClient = getClient(1);
        clientDAO.insert(alreadyInsertedClient);
    }

    private static List<Client> getClientList() {
        return clientList;
    }

    private static Client getClient(long id) {
        return new Client(id,"Test" + id, "User" + id, "test" + id + "@user.com");
    }

    private void initializeInternalList() {
        clientList = new ArrayList<>(Arrays.asList(new Client(1, "Test1", "User1", "test1@user.com"),
                new Client(2, "Test2", "User2", "test2@user.com"),
                new Client(3, "Test3", "User3", "test3@user.com"),
                new Client(4, "Test4", "User4", "test4@user.com"),
                new Client(5, "Test5", "User5", "test5@user.com")));
    }

    private static void createDatabase() {
        handle.begin();
        handle.createScript(FixtureHelpers.fixture("fixtures/db/schema.sql"))
                .execute();
        handle.commit();
    }

    private static void resetDatabase() {
        handle.createScript(FixtureHelpers.fixture("fixtures/db/reset.sql"))
                .execute();
        handle.commit();
    }

}
