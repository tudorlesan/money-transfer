package com.lesan.moneytransfer.db;

import com.codahale.metrics.MetricRegistry;
import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.core.Client;
import com.lesan.moneytransfer.core.enums.Currency;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import io.dropwizard.testing.FixtureHelpers;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.UnableToExecuteStatementException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountDAOTest {

    private static final int TEST_ID_1 = 1;
    private static final int TEST_ID_NOT_FOUND = -999;

    private AccountDAO accountDAO;

    private static List<Account> accountList;
    private static Jdbi jdbi;
    private static Handle handle;

    @BeforeClass
    public static void setUp() {
        Environment env = new Environment("test-env", Jackson.newObjectMapper(), null, new MetricRegistry(), null);

        JdbiFactory factory = new JdbiFactory();
        jdbi = factory.build(env, getDataSourceFactory(), "test");

        handle = jdbi.open();
        createDatabase();
    }

    @AfterClass
    public static void tearDown(){
        resetDatabase();
        handle.close();
    }

    private static DataSourceFactory getDataSourceFactory() {
        DataSourceFactory dataSourceFactory = new DataSourceFactory();
        dataSourceFactory.setDriverClass("org.h2.Driver");
        dataSourceFactory.setUrl("jdbc:h2:mem:testDb");
        dataSourceFactory.setUser("sa");
        dataSourceFactory.setPassword("");
        return dataSourceFactory;
    }

    @Before
    public void setupTests() {
        accountDAO = jdbi.onDemand(AccountDAO.class);
        initializeInternalList();

    }

    @Test
    public void testFindById() {
        assertThat(accountDAO.findByIdWithDetails(TEST_ID_1)).isEqualTo(getAccount(TEST_ID_1));
    }

    @Test
    public void testFindByIdNotFound() {
        assertThat(accountDAO.findByIdWithDetails(TEST_ID_NOT_FOUND)).isNull();
    }

    @Test
    public void testFindAll() {
        assertThat(accountDAO.findAllWithDetails()).isEqualTo(getAccountList());
    }

    @Test
    public void testFindAllPaginated() {
        List<Account> result = accountDAO.findAllWithDetailsPaginated(1, 1);
        assertThat(result).isEqualTo(Collections.singletonList(getAccount(TEST_ID_1)));

        result = accountDAO.findAllWithDetailsPaginated(2, 1);
        assertThat(result).isEqualTo(Collections.singletonList(accountList.get(1)));

        result = accountDAO.findAllWithDetailsPaginated(1, 2);
        assertThat(result).isEqualTo(new ArrayList<>(Arrays.asList(accountList.get(0), accountList.get(1))));

        result = accountDAO.findAllWithDetailsPaginated(1, accountList.size());
        assertThat(result).isEqualTo(accountList);
    }

    @Test
    public void testFindAllPaginatedInvalidInput() {
        List<Account> result = accountDAO.findAllWithDetailsPaginated(-20, 1);
        assertThat(result).isEqualTo(Collections.singletonList(accountList.get(0)));

        result = accountDAO.findAllWithDetailsPaginated(0, 1);
        assertThat(result).isEqualTo(Collections.singletonList(accountList.get(0)));

        result = accountDAO.findAllWithDetailsPaginated(2, 0);
        assertThat(result).isEqualTo(Collections.emptyList());

        result = accountDAO.findAllWithDetailsPaginated(999, 1);
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    public void testInsertAccountSuccess() {
        Client newClient = getAccount(1).getClient();
        Account newAccount = new Account(9, newClient, new BigDecimal("500"), Currency.GBP);

        long id = accountDAO.insert(newAccount, newAccount.getClient().getId());

        Account addedAccount = accountDAO.findByIdWithDetails(id);
        assertThat(addedAccount).isNotNull();
        assertThat(addedAccount.getId()).isEqualTo(id);
        assertThat(addedAccount.getClient()).isEqualTo(newClient);
        assertThat(addedAccount.getBalance()).isEqualTo(newAccount.getBalance());
        assertThat(addedAccount.getCurrency()).isEqualTo(newAccount.getCurrency());
        assertThat(addedAccount.getCreatedOn()).isNotNull();
        handle.execute("delete from accounts where id = (?)", id);
        handle.commit();
    }

    @Test(expected = UnableToExecuteStatementException.class)
    public void testInsertAccountInvalidClient() {
        Account newAccount = new Account(9, null, new BigDecimal("500"), Currency.GBP);
        accountDAO.insert(newAccount, TEST_ID_NOT_FOUND);
    }

    private static void createDatabase() {
        handle.begin();
        handle.createScript(FixtureHelpers.fixture("fixtures/db/schema.sql"))
                .execute();
        handle.commit();
    }
    private static void resetDatabase() {
        handle.createScript(FixtureHelpers.fixture("fixtures/db/reset.sql"))
                .execute();
        handle.commit();
    }

    private static List<Account> getAccountList() {
        return accountList;
    }

    private static Account getAccount(long id) {
        return new Account(id, new Client(id, "Test" + id, "User" + id, "test" + id + "@user.com"), new BigDecimal("100"), Currency.GBP);
    }

    private void initializeInternalList() {
        List<Client> clientList = new ArrayList<>(Arrays.asList(new Client(1, "Test1", "User1", "test1@user.com"),
                new Client(2, "Test2", "User2", "test2@user.com"),
                new Client(3, "Test3", "User3", "test3@user.com"),
                new Client(4, "Test4", "User4", "test4@user.com"),
                new Client(5, "Test5", "User5", "test5@user.com")));

        accountList = new ArrayList<>(Arrays.asList(
                new Account(1, clientList.get(0), new BigDecimal("100"), Currency.GBP),
                new Account(2, clientList.get(0), new BigDecimal("200.07"), Currency.EUR),
                new Account(3, clientList.get(1), new BigDecimal("1300.97"), Currency.USD),
                new Account(4, clientList.get(2), new BigDecimal("25933"), Currency.USD),
                new Account(5, clientList.get(2), new BigDecimal("436.35"), Currency.GBP),
                new Account(6, clientList.get(2), new BigDecimal("9573.2"), Currency.EUR),
                new Account(7, clientList.get(3), new BigDecimal("25432.4"), Currency.EUR),
                new Account(8, clientList.get(4), new BigDecimal("0"), Currency.GBP)
        ));
    }
}
