package com.lesan.moneytransfer;


import com.lesan.moneytransfer.core.Client;
import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(DropwizardExtensionsSupport.class)
class MoneyTransferIntegrationTest {

    private static final String TMP_FILE = createTempFile();
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("test_config.yml");

    private static final DropwizardAppExtension<MoneyTransferConfiguration> RULE = new DropwizardAppExtension<>(
            MoneyTransferApplication.class, CONFIG_PATH,
            ConfigOverride.config("database.url", "jdbc:h2:" + TMP_FILE));
    private static final String CLIENTS_PATH = "/clients";
    private static final String LOCALHOST = "http://localhost:";
    private static final int FOUR_ENTITIES = 4;
    private static final int FIVE_ENTITIES = 5;

    @BeforeAll
    static void initializeDb(){
        IntStream.rangeClosed(1, FOUR_ENTITIES).forEach(n -> RULE.client().target(LOCALHOST + RULE.getLocalPort() + CLIENTS_PATH)
                .request()
                .post(Entity.entity(new Client("Test" + n, "User", "test" + n + "@user.com"), MediaType.APPLICATION_JSON_TYPE)));

    }

    private static String createTempFile() {
        try {
            return File.createTempFile("test_config", null).getAbsolutePath();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Test
    void testGetClients(){
        final List<Client> clientList = RULE.client().target(LOCALHOST + RULE.getLocalPort() + CLIENTS_PATH)
                .request()
                .get(List.class);
        assertThat(clientList.size()).isGreaterThanOrEqualTo(FOUR_ENTITIES);
        assertThat(clientList.size()).isLessThanOrEqualTo(FIVE_ENTITIES);
    }

    @Test
    void testGetClient(){
        final Client client = RULE.client().target(LOCALHOST + RULE.getLocalPort() + CLIENTS_PATH + "/1")
                .request()
                .get(Client.class);
        Client expectedClient = new Client(1, "Test1", "User", "test1@user.com");
        assertThat(client).isEqualTo(expectedClient);
    }

    @Test
    void testPostClient(){
        final Client client = new Client("Test5", "User", "test5@user.com");
        final Client newClient = postClient(client);
        assertThat(newClient.getId()).isNotNull();
        assertThat(newClient.getFirstName()).isEqualTo(client.getFirstName());
        assertThat(newClient.getLastName()).isEqualTo(client.getLastName());
        assertThat(newClient.getEmailAddress()).isEqualTo(client.getEmailAddress());
    }


    private Client postClient(Client client) {
        return RULE.client().target(LOCALHOST + RULE.getLocalPort() + CLIENTS_PATH)
                .request()
                .post(Entity.entity(client, MediaType.APPLICATION_JSON_TYPE))
                .readEntity(Client.class);
    }
}

