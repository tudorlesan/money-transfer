package com.lesan.moneytransfer.resources;

import com.lesan.moneytransfer.core.Client;
import com.lesan.moneytransfer.db.ClientDAO;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.jdbi.v3.core.statement.UnableToExecuteStatementException;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class ClientResourceTest {

    private static final ClientDAO clientDAO = mock(ClientDAO.class);
    private static final String CLIENTS_PATH = "/clients";
    private static final String PAGE = "page";
    private static final String SIZE = "size";
    private static final String FOUND_ID_PATH = "/1";
    private static final String NOT_FOUND_ID_PATH = "/3";
    private static final int DEFAULT_PAGE_INT = 1;
    private static final int DEFAULT_SIZE_INT = 20;

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new ClientResource(clientDAO))
            .build();
    private ArgumentCaptor<Client> clientCaptor = ArgumentCaptor.forClass(Client.class);

    private static List<Client> clientList;

    @Before
    public void setup() {
        initializeInternalList();
        when(clientDAO.findById(eq(1L))).thenReturn(getClient(1));
        when(clientDAO.findById(eq(2L))).thenReturn(getClient(2));
    }

    @Test
    public void getClientSuccess() {
        assertThat(resources.target(CLIENTS_PATH + FOUND_ID_PATH).request().get(Client.class))
                .isEqualTo(clientList.get(0));
        verify(clientDAO).findById(1L);

    }

    @Test(expected = WebApplicationException.class)
    public void getClientNotFound() {
        resources.target(CLIENTS_PATH + NOT_FOUND_ID_PATH).request().get(Client.class);
    }

    @Test
    public void createClient() {
        when(clientDAO.insert(any(Client.class))).thenReturn(1L);
        final Response response = resources.target(CLIENTS_PATH)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(clientList.get(0), MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        verify(clientDAO).insert(clientCaptor.capture());
        assertThat(clientCaptor.getValue()).isEqualTo(clientList.get(0));
    }

    @Test
    public void createClientDuplicateEmails() {
        when(clientDAO.insert(any(Client.class))).thenThrow(UnableToExecuteStatementException.class);
        final Response response = resources.target(CLIENTS_PATH)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(clientList.get(0), MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CONFLICT);
    }

    @Test
    public void getClients() {
        when(clientDAO.findAllPaginated(anyInt(), anyInt())).thenReturn(clientList);

        final List<Client> response = resources.target(CLIENTS_PATH)
                .request().get(new GenericType<List<Client>>() {
                });

        verify(clientDAO).findAllPaginated(DEFAULT_PAGE_INT, DEFAULT_SIZE_INT);
        assertThat(response).containsAll(clientList);
    }

    @Test
    public void getClientsPaginated() {
        when(clientDAO.findAllPaginated(anyInt(), anyInt())).thenReturn(clientList);

        final List<Client> response = resources.target(CLIENTS_PATH).queryParam(PAGE, 2).queryParam(SIZE, 1)
                .request().get(new GenericType<List<Client>>() {
                });

        verify(clientDAO).findAllPaginated(2, 1);
        assertThat(response).containsAll(clientList);
    }

    @Test(expected = WebApplicationException.class)
    public void getClientsPaginatedInvalidPage() {
        when(clientDAO.findAllPaginated(anyInt(), anyInt())).thenReturn(clientList);

        resources.target(CLIENTS_PATH).queryParam(PAGE, 0).queryParam(SIZE, 1)
                .request().get(new GenericType<List<Client>>() {
        });
    }

    private static Client getClient(long id) {
        return new Client(id, "Test" + id, "User" + id, "test" + id + "@user.com");
    }

    private static void initializeInternalList() {
        clientList = new ArrayList<>(Arrays.asList(new Client(1, "Test1", "User1", "test1@user.com"),
                new Client(2, "Test2", "User2", "test2@user.com"),
                new Client(3, "Test3", "User3", "test3@user.com"),
                new Client(4, "Test4", "User4", "test4@user.com"),
                new Client(5, "Test5", "User5", "test5@user.com")));
    }

    @After
    public void tearDown() {
        reset(clientDAO);
    }
}
