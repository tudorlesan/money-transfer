package com.lesan.moneytransfer.resources;

import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.core.Transfer;
import com.lesan.moneytransfer.core.enums.Currency;
import com.lesan.moneytransfer.core.enums.TransferStatus;
import com.lesan.moneytransfer.db.TransferDAO;
import com.lesan.moneytransfer.db.exceptions.TransferCreationException;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class TransferResourceTest {

    private static final long TEST_TRANSFER_SOURCE_ACCOUNT_ID = 1L;
    private static final long TEST_TRANSFER_DESTINATION_ACCOUNT_ID = 5L;
    private static final long TEST_TRANSFER_ID = 1L;
    private static final String TRANSFERS_PATH = "/transfers";
    private static final String PAGE = "page";
    private static final String SIZE = "size";
    private static final String FOUND_ID_PATH = "/1";
    private static final String NOT_FOUND_ID_PATH = "/3";
    private static final int DEFAULT_PAGE_INT = 1;
    private static final int DEFAULT_SIZE_INT = 20;

    private static final TransferDAO transferDAO = mock(TransferDAO.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new TransferResource(transferDAO))
            .build();
    private ArgumentCaptor<Transfer> transferCaptor = ArgumentCaptor.forClass(Transfer.class);

    private static List<Transfer> transferList;

    @Before
    public void setup() {
        initializeInternalList();
        when(transferDAO.findByIdWithDetails(eq(1L))).thenReturn(transferList.get(0));
        when(transferDAO.findByIdWithDetails(eq(2L))).thenReturn(transferList.get(0));
    }

    @Test
    public void getTransferSuccess() {
        assertThat(resources.target(TRANSFERS_PATH + FOUND_ID_PATH).request().get(Transfer.class))
                .isEqualTo(transferList.get(0));
        verify(transferDAO).findByIdWithDetails(TEST_TRANSFER_ID);

    }

    @Test(expected = WebApplicationException.class)
    public void getTransferNotFound() {
        resources.target(TRANSFERS_PATH + NOT_FOUND_ID_PATH).request().get(Transfer.class);
    }

    @Test
    public void createTransfer() throws TransferCreationException {
        when(transferDAO.insert(any(Transfer.class), anyLong(), anyLong())).thenReturn(TEST_TRANSFER_ID);
        final Response response = resources.target(TRANSFERS_PATH)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(transferList.get(0), MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        verify(transferDAO).insert(transferCaptor.capture(), eq(TEST_TRANSFER_SOURCE_ACCOUNT_ID), eq(TEST_TRANSFER_DESTINATION_ACCOUNT_ID));
        assertThat(transferCaptor.getValue()).isEqualTo(transferList.get(0));
    }

    @Test
    public void createTransferInvalidAccount() throws TransferCreationException {
        when(transferDAO.insert(any(Transfer.class), anyLong(), anyLong())).thenThrow(TransferCreationException.class);
        final Response response = resources.target(TRANSFERS_PATH)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(transferList.get(0), MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CONFLICT);
    }

    @Test
    public void getTransfers() {
        when(transferDAO.findAllWithDetailsPaginated(anyInt(), anyInt())).thenReturn(transferList);

        final List<Transfer> response = resources.target(TRANSFERS_PATH)
                .request().get(new GenericType<List<Transfer>>() {
                });

        verify(transferDAO).findAllWithDetailsPaginated(DEFAULT_PAGE_INT,  DEFAULT_SIZE_INT);
        assertThat(response).containsAll(transferList);
    }

    @Test
    public void getTransfersPaginated() {
        when(transferDAO.findAllWithDetailsPaginated(anyInt(), anyInt())).thenReturn(transferList);

        final List<Transfer> response = resources.target(TRANSFERS_PATH).queryParam(PAGE, 2).queryParam(SIZE, 1)
                .request().get(new GenericType<List<Transfer>>() {
                });

        verify(transferDAO).findAllWithDetailsPaginated(2, 1);
        assertThat(response).containsAll(transferList);
    }

    @Test(expected = WebApplicationException.class)
    public void getTransfersPaginatedInvalidPage() {
        when(transferDAO.findAllWithDetailsPaginated(anyInt(), anyInt())).thenReturn(transferList);

        resources.target(TRANSFERS_PATH).queryParam(PAGE, 0).queryParam(SIZE, 1)
                .request().get(new GenericType<List<Transfer>>() {
        });
    }

    private void initializeInternalList() {
        List<Account> accountList = new ArrayList<>(Arrays.asList(
                new Account(1, null, new BigDecimal("100"), Currency.GBP),
                new Account(2, null, new BigDecimal("200.07"), Currency.EUR),
                new Account(3, null, new BigDecimal("1300.97"), Currency.USD),
                new Account(4, null, new BigDecimal("25933"), Currency.USD),
                new Account(5, null, new BigDecimal("436.35"), Currency.GBP),
                new Account(6, null, new BigDecimal("9573.2"), Currency.EUR),
                new Account(7, null, new BigDecimal("25432.4"), Currency.EUR),
                new Account(8, null, new BigDecimal("0"), Currency.GBP)
        ));

        Transfer t1 = new Transfer(1, "test transfer 1", new BigDecimal("50"), TransferStatus.CREATED);
        t1.setSourceAccount(accountList.get(0));
        t1.setDestinationAccount(accountList.get(4));
        Transfer t2 = new Transfer(2, "test transfer 2", new BigDecimal("123.5"), TransferStatus.CREATED);
        t2.setSourceAccount(accountList.get(5));
        t2.setDestinationAccount(accountList.get(1));
        Transfer t3 = new Transfer(3, "test transfer 3", new BigDecimal("1333.45"), TransferStatus.CREATED);
        t3.setSourceAccount(accountList.get(4));
        t3.setDestinationAccount(accountList.get(7));
        Transfer t4 = new Transfer(4, "test transfer 4", new BigDecimal("923.35"), TransferStatus.CREATED);
        t4.setSourceAccount(accountList.get(2));
        t4.setDestinationAccount(accountList.get(3));
        Transfer t5 = new Transfer(5, "test transfer 5", new BigDecimal("5543.5"), TransferStatus.CREATED);
        t5.setSourceAccount(accountList.get(7));
        t5.setDestinationAccount(accountList.get(0));
        transferList = new ArrayList<>(Arrays.asList(t1, t2, t3, t4,t5));
    }

    @After
    public void tearDown() {
        reset(transferDAO);
    }
}
