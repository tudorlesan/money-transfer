package com.lesan.moneytransfer.resources;

import com.lesan.moneytransfer.core.Account;
import com.lesan.moneytransfer.core.Client;
import com.lesan.moneytransfer.core.enums.Currency;
import com.lesan.moneytransfer.db.AccountDAO;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.jdbi.v3.core.statement.UnableToExecuteStatementException;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class AccountResourceTest {

    private static final AccountDAO accountDAO = mock(AccountDAO.class);
    private static final String ACCOUNTS_PATH = "/accounts";
    private static final String PAGE = "page";
    private static final String SIZE = "size";
    private static final String FOUND_ID_PATH = "/1";
    private static final String NOT_FOUND_ID_PATH = "/3";
    private static final int DEFAULT_PAGE_INT = 1;
    private static final int DEFAULT_SIZE_INT = 20;

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new AccountResource(accountDAO))
            .build();
    private ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);

    private static List<Account> accountList;

    @Before
    public void setup() {
        initializeInternalList();
        when(accountDAO.findByIdWithDetails(eq(1L))).thenReturn(getAccount(1));
        when(accountDAO.findByIdWithDetails(eq(2L))).thenReturn(getAccount(2));
    }

    @Test
    public void getAccountSuccess() {
        assertThat(resources.target(ACCOUNTS_PATH + FOUND_ID_PATH).request().get(Account.class))
                .isEqualTo(accountList.get(0));
        verify(accountDAO).findByIdWithDetails(1L);

    }

    @Test(expected = WebApplicationException.class)
    public void getAccountNotFound() {
        resources.target(ACCOUNTS_PATH + NOT_FOUND_ID_PATH).request().get(Account.class);
    }

    @Test
    public void createAccount() {
        when(accountDAO.insert(any(Account.class), anyLong())).thenReturn(1L);
        final Response response = resources.target(ACCOUNTS_PATH)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(accountList.get(0), MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        verify(accountDAO).insert(accountCaptor.capture(), eq(1L));
        assertThat(accountCaptor.getValue()).isEqualTo(accountList.get(0));
    }

    @Test
    public void createAccountInvalidClient() {
        when(accountDAO.insert(any(Account.class), anyLong())).thenThrow(UnableToExecuteStatementException.class);
        final Response response = resources.target(ACCOUNTS_PATH)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(accountList.get(0), MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.BAD_REQUEST);
    }

    @Test
    public void getAccounts() {
        when(accountDAO.findAllWithDetailsPaginated(anyInt(), anyInt())).thenReturn(accountList);

        final List<Account> response = resources.target(ACCOUNTS_PATH)
                .request().get(new GenericType<List<Account>>() {
                });

        verify(accountDAO).findAllWithDetailsPaginated(DEFAULT_PAGE_INT, DEFAULT_SIZE_INT);
        assertThat(response).containsAll(accountList);
    }

    @Test
    public void getAccountsPaginated() {
        when(accountDAO.findAllWithDetailsPaginated(anyInt(), anyInt())).thenReturn(accountList);

        final List<Account> response = resources.target(ACCOUNTS_PATH).queryParam(PAGE, 2).queryParam(SIZE, 1)
                .request().get(new GenericType<List<Account>>() {
                });

        verify(accountDAO).findAllWithDetailsPaginated(2, 1);
        assertThat(response).containsAll(accountList);
    }

    @Test(expected = WebApplicationException.class)
    public void getAccountsPaginatedInvalidPage() {
        when(accountDAO.findAllWithDetailsPaginated(anyInt(), anyInt())).thenReturn(accountList);

        resources.target(ACCOUNTS_PATH).queryParam(PAGE, 0).queryParam(SIZE, 1)
                .request().get(new GenericType<List<Account>>() {
        });
    }

    private static Account getAccount(long id) {
        return new Account(id, new Client(id, "Test" + id, "User" + id, "test" + id + "@user.com"), new BigDecimal("100"), Currency.GBP);
    }

    private void initializeInternalList() {
        List<Client> clientList = new ArrayList<>(Arrays.asList(new Client(1, "Test1", "User1", "test1@user.com"),
                new Client(2, "Test2", "User2", "test2@user.com"),
                new Client(3, "Test3", "User3", "test3@user.com"),
                new Client(4, "Test4", "User4", "test4@user.com"),
                new Client(5, "Test5", "User5", "test5@user.com")));

        accountList = new ArrayList<>(Arrays.asList(
                new Account(1, clientList.get(0), new BigDecimal("100"), Currency.GBP),
                new Account(2, clientList.get(0), new BigDecimal("200.07"), Currency.EUR),
                new Account(3, clientList.get(1), new BigDecimal("1300.97"), Currency.USD),
                new Account(4, clientList.get(2), new BigDecimal("25933"), Currency.USD),
                new Account(5, clientList.get(2), new BigDecimal("436.35"), Currency.GBP),
                new Account(6, clientList.get(2), new BigDecimal("9573.2"), Currency.EUR),
                new Account(7, clientList.get(3), new BigDecimal("25432.4"), Currency.EUR),
                new Account(8, clientList.get(4), new BigDecimal("0"), Currency.GBP)
        ));
    }

    @After
    public void tearDown() {
        reset(accountDAO);
    }
}
