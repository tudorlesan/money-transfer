# MoneyTransfer

Developed using Java 8 and Dropwizard

How to start the MoneyTransfer application
---

1. Run `mvn clean package` to build the application or use wrappers mvnw/mvnw.cmd
2. Start application with `java -jar target\money-transfer-1.0-SNAPSHOT.jar server .\config.yml`
3. To check that the application is running enter url `http://localhost:8082` and see the Operational Menu but the API is up at 8081


## How to use the application
### Clients
#### Create a client
```
    POST localhost:8081/clients
    {
        "firstName": "Mark",
        "lastName": "Smith",
        "emailAddress": "marksmith@gmail.com"
    }

```
Response:
```
    HTTP 200 OK
    {
        "id": 1,
        "firstName": "Mark",
        "lastName": "Smith",
        "emailAddress": "marksmith@gmail.com"
    }
```
or error:
```
    HTTP 409 CONFLICT
    {
        "code": 409,
        "message": "Email already used. Unable to create client"
    }
```
#### Find a client by id
```
    GET localhost:8081/clients/1
```
Response:
```
    HTTP 200 OK
    {
        "id": 1,
        "firstName": "Mark",
        "lastName": "Smith",
        "emailAddress": "marksmith@gmail.com"
    }
```
or error:
```
    HTTP 404 NOT FOUND
    {
        "code": 404,
        "message": "Client not found"
    }
```
#### Find all clients
```
    GET localhost:8081/clients
```
Response:
```
    HTTP 200 OK
    [
        {
            "id": 1,
            "firstName": "Mark",
            "lastName": "Smith",
            "emailAddress": "marksmith@gmail.com",
        },
        {
            "id": 2,
            "firstName": "James ",
            "lastName": "Johnson",
            "emailAddress": "jamesjohnson@gmail.com",
        }

    ]
```
By default, this retrieves the first page of 20 Clients. Use GET localhost:8082/clients?page=x&size=y to change the page and size.
Retrieves empty list [] if there are no records or if the page does not exist.
### Accounts
#### Create an account
```
    POST localhost:8081/accounts
    {
        "client": {
            "id": 1
        },
        "balance": 1200.05,
        "currency": "GBP"
    }
```
Response:
```
    HTTP 200 OK
    {
        "id": 1,
        "client": {
            "id": 1,
            "firstName": "Mark",
            "lastName": "Smith",
            "emailAddress": "marksmith@gmail.com"
        },
        "balance": 1200.05,
        "currency": "GBP",
        "createdOn": 1570209971412
    }
```
or error:
```
    HTTP 400 BAD REQUEST
    {
        "code": 400,
        "message": "Invalid client id for account"
    }
```
#### Find an account by id
```
    GET localhost:8081/accounts/1
```
Response:
```
    HTTP 200 OK
    {
        "id": 1,
        "client": {
            "id": 1,
            "firstName": "Mark",
            "lastName": "Smith",
            "emailAddress": "marksmith@gmail.com"
        },
        "balance": 1200.05,
        "currency": "GBP",
        "createdOn": 1570209971412
    }
```
or error:
```
    HTTP 404 NOT FOUND
    {
        "code": 404,
        "message": "Account not found"
    }
```
#### Find all accounts
```
    GET localhost:8081/accounts
```
Response:
```
    HTTP 200 OK
    [
        {
            "id": 1,
            "client": {
                "id": 1,
                "firstName": "Mark",
                "lastName": "Smith",
                "emailAddress": "marksmith@gmail.com"
            },
            "balance": 1200.05,
            "currency": "GBP",
            "createdOn": 1570209971412
        },
        {
            "id": 2,
            "client": {
                "id": 2,
                "firstName": "James ",
                "lastName": "Johnson",
                "emailAddress": "jamesjohnson@gmail.com",
            },
            "balance": 1200.05,
            "currency": "GBP",
            "createdOn": 1570209971412
        }
    ]
```
By default, this retrieves the first page of 20 Accounts. Use GET localhost:8082/accounts?page=x&size=y to change the page and size.
Retrieves empty list [] if there are no records or if the page does not exist.
### Transfers
#### Create a transfer
```
    POST localhost:8081/transfers
    {
        "description": "First transfer",
        "amount": 100.50,
        "sourceAccount": {
            "id": 1
        },
        "destinationAccount": {
            "id": 2
        }
    }
```
Response:
```
    HTTP 200 OK
    {
        "id": 1,
        "description": "First transfer",
        "amount": 100.5,
        "sourceAccount": {
            "id": 1,
            "client": null,
            "balance": 1200.05,
            "currency": "GBP",
            "createdOn": 1570222800000
        },
        "destinationAccount": {
            "id": 2,
            "client": null,
            "balance": 2590,
            "currency": "GBP",
            "createdOn": 1570222800000
        },
        "status": "CREATED",
        "createdOn": 1570222800000,
        "executedOn": null
    }
```
or errors:
Transfer between invalid accounts -
```
    HTTP 409 CONFLICT
         {
             "code": 409,
             "message": "Account not found"
         }
```
Accounts have different currencies which is not supported currently -
```
    HTTP 409 CONFLICT
    {
        "code": 409,
        "message": "Source account and destination account have different currencies. Transfer between accounts with different currencies is not supported at the moment"
    }
```
#### Find a transfer by id
```
    GET localhost:8081/transfers/1
```
Response:
```
    HTTP 200 OK
    {
        "id": 1,
        "description": "First transfer",
        "amount": 100.5,
        "sourceAccount": {
            "id": 1,
            "client": null,
            "balance": 1200.05,
            "currency": "GBP",
            "createdOn": 1570222800000
        },
        "destinationAccount": {
            "id": 3,
            "client": null,
            "balance": 2590,
            "currency": "GBP",
            "createdOn": 1570222800000
        },
        "status": "CREATED",
        "createdOn": 1570222800000,
        "executedOn": null
    }
```
or error:
```
    HTTP 404 NOT FOUND
    {
        "code": 404,
        "message": "Transfer not found"
    }
```

#### Find all transfers
```
    GET localhost:8081/transfers
```
Response:
```
    HTTP 200 OK
    [
        {
            "id": 1,
            "description": "First transfer",
            "amount": 100.5,
            "sourceAccount": {
                "id": 1,
                "client": null,
                "balance": 1200.05,
                "currency": "GBP",
                "createdOn": 1570222800000
            },
            "destinationAccount": {
                "id": 3,
                "client": null,
                "balance": 2590,
                "currency": "GBP",
                "createdOn": 1570222800000
            },
            "status": "CREATED",
            "createdOn": 1570222800000,
            "executedOn": null
        }
    ]
```
By default, this retrieves the first page of 20 Accounts. Use GET localhost:8082/transfers?page=x&size=y to change the page and size.
Retrieves empty list [] if there are no records or if the page does not exist.
#### Delete a transfer
```
    DELETE localhost:8081/transfers/1
```
Response:
```
    HTTP 200 OK
```
or error:
```
    HTTP 404 NOT FOUND
    {
        "code": 404,
        "message": "Transfer not found"
    }
```
#### Execute a transfer
```
    GET localhost:8081/transfers/execution/1
```
Response:
```
    HTTP 200 OK
    {
        "id": 2,
        "description": "First transfer",
        "amount": 100.5,
        "sourceAccount": {
            "id": 1,
            "client": null,
            "balance": 1099.55,
            "currency": "GBP",
            "createdOn": 1570222800000
        },
        "destinationAccount": {
            "id": 3,
            "client": null,
            "balance": 2690.50,
            "currency": "GBP",
            "createdOn": 1570222800000
        },
        "status": "EXECUTED",
        "createdOn": 1570222800000,
        "executedOn": 1570222800000
    }
```
or errors:
Transfer to be executed not found -
```
    HTTP 404 NOT FOUND
    {
        "code": 404,
        "message": "Transfer not found"
    }
```
Transfer was already executed -
```
    HTTP 409 CONFLICT
    {
        "code": 409,
        "message": "Transfer was already executed on: 2019-10-05"
    }
```
Transfer unable to be executed -
```
    HTTP 409 CONFLICT
    {
        "code": 409,
        "message": "Source account has insufficient funds to execute the transfer"
    }
```